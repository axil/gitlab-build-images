## Gemfiles

The gemfiles are copied from:

- https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/Gemfile
- https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/Gemfile.lock


```sh
wget -O Gemfile https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/Gemfile
wget -O Gemfile.lock https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/Gemfile.lock
```

They are then used by `Dockerfile.www-gitlab-com-alpine` to install the gems
needed.

## Docker base image

We're using [teradiot/alpine-ruby-libv8](https://hub.docker.com/r/teradiot/alpine-ruby-libv8/)
as the base image and then we install the gems inside the image itself for
faster build times.
