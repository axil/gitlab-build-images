This repository is used to build docker images that are used to build and test
various parts of GitLab:

1. Build Omnibus packages
1. Test GitLab-CE/EE project
1. Build gitlab-com/www-gitlab-com project
